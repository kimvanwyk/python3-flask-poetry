# python3-flask-poetry
A Docker image to add [poetry](https://poetry.eustace.io) support via [ONBUILD](https://docs.docker.com/engine/reference/builder/#onbuild) to Sebastián Ramírez's excellent [meinheld-gunicorn-flask](https://hub.docker.com/r/tiangolo/meinheld-gunicorn-flask) [Flask/Meinheld/GUnicorn](https://flask.palletsprojects.com/) image.

# Usage

* Use this image as the **FROM** argument in a container.

* Copy all source files to */app/* in the container

* Ensure there is a *pyproject.toml* in the root directory - it will be copied into the directory and all contents **poetry install**ed into the container.

* The container will expect to find a Flask application called *app* in a file called *main.py*. To use a different file name, set the *MODULE_NAME* env variable to the desired file name. For eg:

    ```
    ENV MODULE_NAME=app
    ```

For full usage details of the comprehensive underlying *uvicorn-gunicorn-fastapi* image, see its [Docker Hub repo](https://hub.docker.com/r/tiangolo/meinheld-gunicorn-flask).

# Example Child Project

## Project Directory

```
- Dockerfile
- pyproject.toml
- app/
     - main.py
     - other_python_source.py
```

## Dockerfile

``` docker
FROM python3-flask-poetry:latest

COPY app/* /app/

```

